﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Core;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using test_hangfire_service.Services;

namespace test_hangfire_service
{
    class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                var isService = !(Debugger.IsAttached || args.Contains("--console"));
                args = args.Where(arg => arg != "--console").ToArray();
                IWebHost builder;

                if (isService)
                {
                    var pathToExe = Process.GetCurrentProcess().MainModule.FileName;
                    var pathToContentRoot = Path.GetDirectoryName(pathToExe);
                    builder = BuildWebHost(args, pathToContentRoot);
                }
                else
                    builder = BuildWebHost(args);

                if (isService)
                {
                    builder.RunAsService();
                }
                else
                {
                    builder.Run();
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            } 
        }

        public static IWebHost BuildWebHost(string[] args, string CurrentDirectory = null)
        {
            if (CurrentDirectory == null)
                CurrentDirectory = Directory.GetCurrentDirectory();

            var builder = new ConfigurationBuilder()
                         .SetBasePath(CurrentDirectory)
                         .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            var Configuration = builder.Build();
             
            var iWeb = WebHost.CreateDefaultBuilder(args)

                 .UseKestrel(x =>
                 {
                     x.ListenAnyIP(Configuration.GetValue<int>("AppSetting:PortableServerPort"));
                 })
                 .UseContentRoot(CurrentDirectory)
                 .UseIISIntegration()
                 .ConfigureAppConfiguration((hostingContext, config) =>
                 {
                     var env = hostingContext.HostingEnvironment;
                     config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                     config.AddEnvironmentVariables();
                 })
                 .UseStartup<Startup>()
                 .Build();

            return iWeb;
        }

    }
}
