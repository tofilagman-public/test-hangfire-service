﻿using Hangfire;
using Hangfire.Common;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using test_hangfire_service.Interfaces;

namespace test_hangfire_service.Services
{
    public class AutomationService : IAutomationService
    {
        private ILogger Logger;

        public AutomationService(ILogger logger)
        {
            Logger = logger.ForContext(typeof(AutomationService));
        }

        public async Task Initiate()
        {
            await Task.Run(() =>
            {
                try
                {
                    var manager = new RecurringJobManager();

                    var job = Job.FromExpression<IAutomationTaskService>(sr => sr.Insert(null));
                    //you can call and schedule any method or object that is registered in scope
                    //dont worry about performcontext, it will replace by hangfire

                    manager.AddOrUpdate("Insert", job, "*/16 * * * *");
                    //every 16 minutes
                    //https://crontab.guru/#*/16_*_*_*_*
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                }
            });
        }

    }

}
