﻿using Dapper;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Linq;
using test_hangfire_service.Dto;
using test_hangfire_service.Interfaces;

namespace test_hangfire_service.Services
{
    public class MySqlService : IMySqlService
    {
        private string sqlConnection;

        public MySqlService(IConfigService config)
        {
            this.sqlConnection = config.MySqlConnection;
        }

        public List<Dtr> FetchDtr()
        {
            using (var conn = new MySqlConnection(this.sqlConnection))
            {
                return conn.Query<Dtr>("Select * from Dtr").ToList();
            }
        }

    }
}
