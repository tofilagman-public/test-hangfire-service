﻿using Hangfire;
using Hangfire.Console;
using Hangfire.Server;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using test_hangfire_service.Interfaces;

namespace test_hangfire_service.Services
{
    public class AutomationTaskService : JobActivator, IAutomationTaskService
    {
        private IMySqlService Sql;
        private ISqlServerService SqlServer;
        private ILogger Logger;

        public AutomationTaskService(IMySqlService sql, ILogger logger, ISqlServerService sqlServer)
        {
            Sql = sql;
            SqlServer = sqlServer;
            Logger = logger.ForContext<AutomationTaskService>();
        }

        public void Execute()
        {

        }

        public void Insert(PerformContext context)
        {
            context.WriteLine("Fetch data from Mysql");
            var dtrs = Sql.FetchDtr();

            if (dtrs.Count > 0)
            {
                context.WriteLine($"Found { dtrs.Count } Records");
                context.WriteLine($"Inserting to SQL Server");
                SqlServer.InsertDtr(dtrs); 
            }
            else
            {
                context.WriteLine($"No Records Found.");
            }

            context.WriteLine("Done");
        }
    }
}
