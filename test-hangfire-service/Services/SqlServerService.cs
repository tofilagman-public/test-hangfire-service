﻿using System;
using System.Collections.Generic;
using System.Text;
using test_hangfire_service.Database;
using test_hangfire_service.Dto;
using test_hangfire_service.Interfaces;

namespace test_hangfire_service.Services
{
    public class SqlServerService : ISqlServerService
    {
        private string sqlConnection;
        private ApplicationDb applicationDb;

        public SqlServerService(IConfigService config, ApplicationDb db)
        {
            this.sqlConnection = config.SQLConnection;
            this.applicationDb = db;
        }

        public void InsertDtr(List<Dtr> dtrs)
        {
            this.applicationDb.Dtr.AddRange(dtrs);
            this.applicationDb.SaveChanges();
        }
    }
}
