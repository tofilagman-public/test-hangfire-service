﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using test_hangfire_service.Interfaces;

namespace test_hangfire_service.Services
{
    public class ConfigService : IConfigService
    {
        public int PortableServerPort { get; set; }
        public string SQLConnection { get; set; }
        public int BackgorundWorkerCount { get; set; }

        public string MySqlConnection { get; set; }

        public void Setup(IConfigurationSection Section)
        {
            PortableServerPort = Section.GetValue<int>("PortableServerPort");
            SQLConnection = Section.GetValue<string>("SQLConnection");
            MySqlConnection = Section.GetValue<string>("MySQLConnection");
        }
    }
}
