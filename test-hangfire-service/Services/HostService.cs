﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;

namespace test_hangfire_service.Services
{
    public class HostService : WebHostService
    {
        public HostService(IWebHost host) : base(host)
        {
        }

        protected override void OnStarting(string[] args)
        {
            base.OnStarting(args);
        }

        protected override void OnStarted()
        {
            base.OnStarted();
        }

        protected override void OnStopping()
        {
            base.OnStopping();
        }
    }

    public static class HostServiceExtensions
    {
        public static void RunAsService(this IWebHost host)
        {
            var webHostService = new HostService(host);
            ServiceBase.Run(webHostService);
        }
    }

}
