﻿using Hangfire;
using Hangfire.Console;
using Hangfire.MySql.Core;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.Data;
using System.Threading;
using test_hangfire_service.Database;
using test_hangfire_service.Interfaces;
using test_hangfire_service.Services;

namespace test_hangfire_service
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        private IHostingEnvironment HostingEnv;

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            HostingEnv = env;
        }

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddHttpContextAccessor();

            string connectionString = Configuration.GetValue<string>("AppSetting:SQLConnection");
            string mysqlConnectionString = Configuration.GetValue<string>("AppSetting:MySQLConnection");

            //add Sql connection through Entity Framework (Recommended)
            services.AddDbContext<ApplicationDb>(options =>
            {
                options.UseSqlServer(connectionString);
            });

            services.AddSingleton(c => Log.Logger); 
            services.AddSingleton<IConfigService, ConfigService>();

            //register Services
            services.AddScoped<IMySqlService, MySqlService>();
            services.AddScoped<ISqlServerService, SqlServerService>();
            services.AddScoped<IAutomationService, AutomationService>();
            services.AddScoped<IAutomationTaskService, AutomationTaskService>();

            //setup hangfire
            services.AddHangfire(configuration =>
             configuration.SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
             .UseSimpleAssemblyNameTypeSerializer()
             .UseRecommendedSerializerSettings()
             .UseStorage(
                 new MySqlStorage(mysqlConnectionString, new MySqlStorageOptions
                 {
                     TransactionIsolationLevel = IsolationLevel.ReadCommitted,
                     QueuePollInterval = TimeSpan.FromSeconds(15),
                     JobExpirationCheckInterval = TimeSpan.FromHours(1),
                     CountersAggregateInterval = TimeSpan.FromMinutes(5),
                     PrepareSchemaIfNecessary = true,
                     DashboardJobListLimit = 50000,
                     TransactionTimeout = TimeSpan.FromMinutes(1),
                     TablePrefix = "Hangfire"
                 })) 
                .UseConsole()
             );

            services.AddHangfireServer();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHangfireDashboard();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}/{id?}",
                    defaults: new { controller = "Route", action = "Index" }
                    );
            });

            //execute the task from scope
            ExecuteHangfire(app.ApplicationServices, CancellationToken.None);
        }

        private async void ExecuteHangfire(IServiceProvider services, CancellationToken cancellationToken)
        {
            //this is the right way, dont use classic initiation eg. new TestClass();
            using (var scope = services.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var automation = scope.ServiceProvider.GetRequiredService<IAutomationService>();
                var config = scope.ServiceProvider.GetRequiredService<IConfigService>();

                config.Setup(Configuration.GetSection("AppSetting")); //note this must be a singleton to take effect in scopes
                await automation.Initiate();
            }
        }
    }

}
