﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace test_hangfire_service.Interfaces
{
    public interface IAutomationService
    {
        Task Initiate();
    }
}
