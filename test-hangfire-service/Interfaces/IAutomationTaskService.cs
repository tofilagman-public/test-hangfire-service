﻿using Hangfire.Server;
using System;
using System.Collections.Generic;
using System.Text;

namespace test_hangfire_service.Interfaces
{
    public interface IAutomationTaskService
    {
        void Execute();
        void Insert(PerformContext context);
    }
}
