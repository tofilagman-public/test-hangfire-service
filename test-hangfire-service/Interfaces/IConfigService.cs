﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace test_hangfire_service.Interfaces
{
    public interface IConfigService
    {
        void Setup(IConfigurationSection Section);

        int PortableServerPort { get; set; }
        string SQLConnection { get; set; }
        int BackgorundWorkerCount { get; set; }

        string MySqlConnection { get; set; }

    }
}
