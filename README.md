* Setup *

Use this SQL Script

```sql
USE master
GO

CREATE DATABASE test_migratelogs
GO

USE test_migratelogs
GO

CREATE TABLE dtr (ID BIGINT IDENTITY(1,1) PRIMARY KEY, AccessNo BIGINT, LogDateTime DATETIME, LogType INT, DateInserted DATETIME DEFAULT CURRENT_TIMESTAMP)
```

* Notes *
1. Update the AppSettings.json

* Step *

1. Go to Appsettings.json to configure connection
2. start debugging or run 

```powershell
PS> dotnet test-hangfire-service.dll --console
```

3. you can register the dll as service because the code use it check HostService.cs
4. Open your browser and locate //locahost:{port}/hangfire
